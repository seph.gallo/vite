import { Node } from 'reactflow';

export const initialNodes: Node[] = [

  {
    id: 'edges-1',
    type: 'input',
    data: { label: 'Input 1' },
    position: { x: 250, y: 0 },
  },
  { id: 'edges-2', data: { label: 'SBOL2' }, position: { x: 150, y: 100 } },
  { id: 'edges-2a', data: { label: 'EAPI' }, position: { x: 0, y: 180 } },
  { id: 'edges-3', data: { label: 'CRM' }, position: { x: 250, y: 200 } },
  { id: 'edges-4', data: { label: 'Falcon' }, position: { x: 400, y: 300 } },
  { id: 'edges-3a', data: { label: 'Guava' }, position: { x: 150, y: 300 } },
  { id: 'edges-5', data: { label: 'Primero' }, position: { x: 250, y: 400 } },
  {
    id: 'edges-6',
    type: 'output',
    data: { label: 'EMS' },
    position: { x: 50, y: 550 },
  },
  {
    id: 'edges-7',
    type: 'output',
    data: { label: 'O365' },
    position: { x: 250, y: 550 },
  },
  
];
  