import { useCallback, useEffect, useState } from 'react'
import './App.css'
import ReactFlow, { useNodesState, useEdgesState, addEdge } from 'reactflow';
import 'reactflow/dist/style.css';

import "./styles.css";

import { initialNodes } from './nodes';
import { initialEdges } from './edges';

type Monitor = {
  created: Date;
  id: number;
  name: string;
  priority: number;
  overall_state: string;
  overall_state_modified: Date;
}

type IMonitor = {
  name: string,
      status: string,
      level: string,
      category: string, 
      application: string,
      issue: string,
      createdAt: string,
      id: number,
      priority: number,
      overallStateModified: Date
}

async function getSongs(): Promise<IMonitor[]> {

  const response = await fetch("https://proxy.cors.sh/https://api.datadoghq.com/api/v1/monitor?group_states=alert&monitor_tags=sbc_level:level1,sbc_env:production" , {
    method: "GET",
    mode: "cors",
    headers: {
      'DD-API-KEY': '477d7ee0109434e42f6a9a55902259cd',
      'DD-APPLICATION-KEY':'517467df6dd3a54bae2f05dff451c9261eb2e181',
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json',
    },
}).then(response => response.json()).then(response => {

  const result = response.filter((a: Monitor) => a.overall_state === 'Alert').map((monitor: Monitor) => (
    { 
      name: monitor?.name, 
      status: monitor?.overall_state, 
      level: monitor?.name?.split('-').at(0), 
      category: monitor?.name?.split('-').at(1), 
      application: monitor?.name?.split('-').at(2), 
      issue: monitor?.name?.split('-').at(3),
      createdAt: monitor?.created,
      id: monitor?.id,
      priority: monitor?.priority,
      overallStateModified: monitor?.overall_state_modified
    }
  ));
  return result;
});

return response;
}


function App() {
  const [songs, setSongs] = useState<[] | IMonitor[]>([]);
  const [nodes, , onNodesChange] = useNodesState(initialNodes);
  const [edges, setEdges, onEdgesChange] = useEdgesState(initialEdges);
  const onConnect = useCallback((params: any) => setEdges((eds: any) => addEdge(params, eds)), []);

  // useEffect(() => {
  //   const interval = setInterval(() => {
  //     (async () => {
  //       const songs = await getSongs();
  //       setSongs(songs);
        
  //     })();;
  //   }, 10000);  
  //   return () => clearInterval(interval);
  // }, []);

  useEffect(() => {
    (async () => {
      const songs = await getSongs();
      setSongs(songs);

    })();
  }, []);

  console.log(songs);

  return (
    <ReactFlow
      nodes={nodes}
      edges={edges}
      onNodesChange={onNodesChange}
      onEdgesChange={onEdgesChange}
      onConnect={onConnect}
      snapToGrid={true}
      fitView
      attributionPosition="top-right"
    />
  )
}

export default App
